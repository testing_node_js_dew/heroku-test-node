import User from '../../../Core/Schemas/User';


async function saveData (args) {
    const NewUser = User;
    const user = new NewUser(args);
    try {
        const saved = await user.save();
        console.log(saved);
        if (saved) {
            return {msg:"Your bee has been saved!"};
        }
    } catch (error) {
        console.error(error);
        throw error;
    }  
}

async function getUser (arg) {
    try {
        const ex_user = await User.findOne({ email: arg });
        return ex_user;
    } catch (error) {
        
    }
}
// getUser= async (arg)=>{
//     User.findOne({ email: arg }, async (err, result) => {
//         if (result) {
//             console.log('result1',result);
//             const rs = await result
//             return  rs;
//         } else {
//             console.log('err',err);
//             return err;
//         }
//     });
// }

export default {
    saveData,
    getUser
};