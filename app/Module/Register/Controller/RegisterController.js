import PasswordHashProvider from '../../../core/providers/PasswordHashProvider'
import userSrvis from '../Services/UserService'
import User from '../../../Core/Schemas/User';

async function createUser(req, res) {
    console.log(req.body);
    if (!req.body.first_name || req.body.first_name == "") {
        return res.status(400).json('EMPTY_FIRST_NAME')
    }
    if (!req.body.email || req.body.email == "") {

        return res.status(400).json('EMPTY_EMAIL')
    }
    if (req.body.Authenticate_type === '' || req.body.Authenticate_type === undefined) {
        return res.status(400).json('EMPTY_Auth_TYpe')
    } else {
        if (req.body.Authenticate_type === 'FORM') {
            if (!req.body.password || req.body.password == "") {
                return res.status(400).json('EMPTY_PASSWORD')
            }
        }
    }

    const data = req.body;

    const hashedPassword = await PasswordHashProvider.createPasswordHash(req.body.password);
    data.password = hashedPassword.password;

    let ex_user = null;
    ex_user = await userSrvis.getUser();
    if (ex_user === null || ex_user === undefined) {
        const setData = await userSrvis.saveData(req.body);
        return res.status(200).json(setData);
    } else {
        return res.status(400).json('EMAIL_EXIST');
    }
}

export default {
    createUser
};