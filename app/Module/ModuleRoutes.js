import express from 'express';
var router = express.Router();
import registerController from '../Module/Register/Controller/RegisterController';
/* GET home page. */
router.get("/", (req, res) => {
  res.json({ message: "API Working..." });
});

router.post("/post",registerController.createUser);

module.exports = router;
