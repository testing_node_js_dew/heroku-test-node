export class ModelBase {

    constructor(model, decorator = null) {
        this.model = model;
        this.decorator = decorator;
    }

    /**
     * Set Data set
     * this data set can be Query some palace
     */
    setData = (data) => {

        this.data = data;
        return this;
    }

    setFields = (fields) => {

        this.fields = undefined;

        if (fields.length > 0) {

            this.fields = fields;
        }

        return this;
    }

    setPopulate = (populate) => {

        this.populate = populate;
        return this;
    }

    sortBy = (sort) => {

        this.sort = sort;
        return this;
    }

    paginate = (page, limit) => {

        const _page = Math.max(0, page - 1);

        this.limit = limit;

        this.page = _page * this.limit;

        return this;
    }


    save = async() => {

        try {

            const query = new this.model(this.data);

            return await query.save();

        } catch (error) {
            console.log(error);
            throw error;
        }

    }



    /**
     * Find One Record
     */
    findOne = async() => {
        try {

            const query = this.model.findOne(this.data);

            if (this.fields) {
                query.select(this.fields.join(' '))
            }
            if (typeof this.populate != 'undefined') {

                query.populate(this.populate);
            }

            query.lean();
            const result = await query.exec();

            return (this.decorator) ? this._decorator(result) : result;



        } catch (error) {
            console.log(error);
            throw error;
        }
    }


    /**
     * Decorate Result
     */
    _decorate = (result) => {

        return new this.decorator(result);

    }

    addBulk = async() => {
        try {

            return await this.model.create(this.data);

        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    updateAll = async(query, options = null) => {
        try {

            return await this.model.updateMany(query, this.data, options);
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    /**
     * Find Records
     */
    findRecords = async() => {
        try {

            const query = this.model.find(this.data);

            if (typeof this.fields != 'undefined') {

                query.select(this.fields.join(' '))
            }

            if (typeof this.populate != 'undefined') {

                query.populate(this.populate);
            }

            if (typeof this.sort != 'undefined') {

                query.sort(this.sort);
            }

            if (typeof this.page != 'undefined') {

                query.skip(this.page).limit(this.limit);

            }
            query.lean();
            const result = await query.exec();

            return (this.decorator) ? this._decorator(result) : result;



        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    countRecords = async() => {
        try {

            const query = this.model.countDocuments();
            query.lean();
            const result = await query.exec();

            return result;



        } catch (error) {
            console.log(error);
            throw error;
        }
    }


    update = async(query, options = null) => {
        try {

            return await this.model.updateOne(query, this.data, options);
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
    deleteMany = async(query, options = null) => {
        try {

            return await this.model.deleteMany(query);
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

}