
const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
  Authenticate_type: {
        type: String,
        required: true,
    },

    first_name: {
        type: String,
        required: true,
    },

    last_name: {
        type: String
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String
    },
    user_type: {
        type: String
    },
    status: {
        type: String,
        default: 'Not active'
    }
});

// export model user with UserSchema
module.exports = mongoose.model('User', UserSchema);