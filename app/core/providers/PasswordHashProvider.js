import bcrypt from 'bcryptjs'
export default class PasswordHashProvider{
	static generatePasswordSalt = async () =>{
		return bcrypt.genSaltSync(20);
	}
	static createPasswordHash = async (password)=>{
        return  {
			salt: await PasswordHashProvider.generatePasswordSalt(),
			password: bcrypt.hashSync(password)
		}
	}
	static comparePassword = async (password, passwordHash)=>{
		return bcrypt.compareSync(password, passwordHash);
	}


}