const mongoose = require("mongoose");
const MONGO_URI = 'mongodb+srv://AjmAdmin:ajm123@ajm-ezdq7.mongodb.net/test';
const InitiateMongoServer = async () => {
    try{
        console.log('mongodb+srv://AjmAdmin:<password>@ajm-ezdq7.mongodb.net/test')
        mongoose.connect(MONGO_URI,
            {
              useUnifiedTopology: true,
              useNewUrlParser: true
            }
        );
    }catch(ex){
        console.log(ex)
    }
  };
  
  module.exports = InitiateMongoServer;