var express = require('express');
var router = express.Router();
const indexRouter = require('../app/Module/ModuleRoutes');
/* GET home page. */
router.get("/", (req, res) => {
  res.json({ message: "API Working" });
});

router.use('/api', indexRouter);

module.exports = router;
